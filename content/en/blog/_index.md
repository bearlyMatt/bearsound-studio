---
title: "sonicSignatures"
weight: "7"
slug: sonicSignatures
---
Welcome to the bearSound Studio blog; Sonic Signatures. Here you can find articles to help you in your quest to make your audio sound fantastic, and deep-dives into the world of audio in society. Topics range from *Recording Vocals 101*, to the *Experiencing Ambience* series where Matt takes a look at the emergence of the Ambient music genre, and how that has impacted our lives to this day.
