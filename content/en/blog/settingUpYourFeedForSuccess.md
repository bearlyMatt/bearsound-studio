---
title: "Setting up an RSS feed for **success**!"
date: 2021-04-01T20:00:00
showDate: true
draft: false
tags: ["blog","RSS","Tutorial","soundcloud","spotify","apple podcast","pocketcast","stitcher","fyyd.de"]
---
Welcome to the first rendition in bearSound Studio's *tutorial series*! So you've come up with an amazing idea, nailed the recordings, edited your podcast to perfection, but now you're being asked to shell out your hard-earned cash for a dedicated hosting platform. Well, we have good news, when you're starting out, you don't need to. You can get yourself a freely hosted RSS feed which we can configure to push to all your favourite podcatchers! Sounds simple right? Let's dig in.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Soundcloud is your friend](#soundcloud-is-your-friend)
    - [Set-up](#set-up)
        - [Important Notes](#important-notes)
    - [Congratulations](#congratulations)
- [Spotify](#spotify)
- [Stitcher](#stitcher)
- [Apple Podcasts](#apple-podcasts)
- [Pocket Casts](#pocket-casts)
- [BONUS: fyyd.de](#bonus-fyydde)
- [BONUS: Google Podcasts](#bonus-google-podcasts)
- [Conclusion](#conclusion)

<!-- markdown-toc end -->


# Soundcloud is your friend
If you've ever had any exposure to indie music, you might have already heard of Soundcloud; a distribution service for audio which caters to amateurs and big-names alike. What we're really after is an oft-overlooked feature - the built in RSS feed.

![soundcloudResized.png](https://img.bearlymedia.com/images/2021/04/01/soundcloudResized.png)

## Set-up
We're going to assume you know how to sign up for a service, so don't worry, none of that stuff. After registering your account, we need to ensure that we have a valid RSS feed for the account. It sounds difficult, but it's really not.

### Important Notes
- Set your profile picture in 3000x3000px size (this will make Apple Podcasts very happy)
	- If you need resizing tools check out [ImageMagick](https://imagemagick.org/index.php) and specifically it's resize command &dagger;
- Your Soundcloud track descriptions will often be used as shownotes (I'm looking at you Apple Podcasts!)

Once logged in, make your way to the three dots in the right-hand corner, go to settings, and choose the content tab.

![soundcloudRSSResize.gif](https://img.bearlymedia.com/images/2021/04/01/soundcloudRSSResize.gif)

From here we get the options needed to configure our very own RSS feed.
 - Custom feed title = Name of your Podcast (i.e. Smith talks with Smith)
 - Custom author name = Creator's name (i.e. Joe Bloggs)
 - Category = Podcast category for podcatchers
 - Language = Self-explanatory
 - Email address displayed = Email address associated with a podcast&#2020;
 - Stats-service URL prefix = Only necessary if you're using a stats-service (like Podtrac)
 - Subscriber redirect = if you're using Soundcloud as a proxy, insert your end-point here.
 - If your podcast contains explicit content **don't forget to check the box!**
 
To save you time (and remember, your time is precious), if you're only using your Soundcloud account to push your podcast, you can check the *Include in RSS feed* box (this will automatically add any new tracks uploaded to your account to the RSS feed).

Lastly is the Creative Commons section. This part is entirely up to you, and I do recommend any content creator gets familiar with Creative Commons licensing. All of bearSound Studio's tutorials series and resources are published with a Creative Commons CC-BY-SA 4.0 license, along with productions such as *bearly a podcast!*, *Roll for Mischief* and some of *bearlyMatt's* output is also released under this license. If you've ever utilised Open Source technologies (like *Audacity* for example), pass the torch on and pop a CC license on your work rather than a Copyright All Rights Reserved.

## Congratulations
You now have a functional RSS feed you can publish your podcast on! But that's only the first part of your journey, now we need to point *Apple Podcasts*, *Spotify*, *Stitcher*, and *Pocketcasts* to your shiny new podcast, and now that we have our RSS feed, it'll take two seconds per podcatcher.

# Spotify
This one is super-easy, especially if you already have a Spotify Account. Head over to the [Spotify for Podcasters](https://podcasters.spotify.com) webpage, log in (or sign up) and go to the three-dot menu at the top left of the window. Hit *Add or Claim your Podcast* from the dropdown menu, and on the following screen, drop your RSS feed into the text box. Double-check that the info is correct, then hit *next*. If all is well, you'll be sent to an email verification screen where an 8-digit code is sent to the email we entered on Soundcloud's RSS page.

Once you've done the deed, you get to add a couple extra bits and pieces - fill them out and you'll be golden.

![spotifyResize.gif](https://img.bearlymedia.com/images/2021/04/01/spotifyResize.gif)

# Stitcher
 This is the easiest of the podcatchers to set up; head over to [Stitcher](https://www.stitcher.com/partners) and the partner portal, sign up and plop your RSS feed in when prompted. Complete the email verification. Boom. Done.
 
 ![stiterResized.png](https://img.bearlymedia.com/images/2021/04/01/stiterResized.png)
 
# Apple Podcasts
We were actually quite surprised with this one, it's not that hard! Head over to [Apple Podcast Connect](https://podcastsconnect.apple.com/login) and log in, or sign-up. Under *My Podcasts* hit the blue plus button and insert your RSS feed. If you've followed the points for Soundcloud's profile picture, then you should be able to hit the *Validate* button in the top right with no errors. If all is well, you can hit *Submit* and a verification email should be sent to the associated email address. That's another one done and dusted.

![appleResize.gif](https://img.bearlymedia.com/images/2021/04/01/appleResize.gif)


# Pocket Casts
Another delightfully easy one here, head over to [PocketCasts](https://www.pocketcasts.com/submit/) and throw in your RSS feed. Decide whether you want this to be a public, or private feed (super useful if you have Patreon-only streams etc.) and you should be good to go!

# BONUS: fyyd.de
This one is a little bit weird, but worth it if you're an opensource advocate. fyyd.de is the podcast indexer of the Nextcloud Podcast client, so if you're hosting your own cloud via Nextcloud, you can simply just type the name of your podcast into the search bar and viola, it'll pop up. 
Go to [fyyd.de/add-feed](https://fyyd.de/add-feed) and paste. Your podcast will feature in the newly added section usually within 24hrs.

# BONUS: Google Podcasts
Guess what? You don't need to do a thing! As our Google overlords scrape the internet indexing it for search results, it will automatically add your podcast to the library!

# Conclusion
Honestly, once you've set up your feed, adding it to services is much of a muchness. We've now made sure that your podcast is accessible to the majority of major podcatchers and given your podcast the best exposure to services you can - without having to pay!

Next stop, marketing!

~ Matt ~

&dagger; ImageMagick is a CLI tool (command line such as Windows Powershell, OSx's Terminal, or GN/Linux's Terminal) which allows you to manipulate image files in a simple and effective way.  [Download](https://imagemagick.org/script/download.php#windows) the binaries and follow the instructions, or, if you're lucky enough to be on Debian/Ubuntu based distros or run Homebrew on Mac `sudo apt install imagemagick`, or `brew install imagemagick` respectively.  After a successfull installation, ensure your terminal is in the directory of the image you want to  resize and run `convert imageToConvert.png -resize 3000x3000 convertedImage.png`, or with Windows, ensure you prepend the command with `magick` or you'll get a slew of errors as Windows has a native command called `convert`.


 
