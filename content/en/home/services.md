---
title: "Services"
weight: 3
slug: services
---
## Post-Production
bearSound studio prides itself on it’s commitment to great sounding, professional audio. That’s why there is no tiered post-production solutions, just the same quality of service regardless whether it is a one off job, or a recurring subscription.

So, if you choose bearSound Studios for your podcast, you can expect as standard:

| *Standard* | *Premium* |
| :-------- | :-------: |
| Editing of podcast to the House Style |N/A |
| De-noising |N/A |
| De-Essing |N/A |
| EQ-ing |N/A |
| Dynamic Processing |N/A |
| Plosive-taming |N/A |
| Silence Removal | N/A| 
| Summing of individual tracks |N/A |
| Mixing of dialogue, music and (unpaid) advertisement |N/A |
| Intro/outro addition |N/A |
| Mastering |N/A |
| Compliance with EBU R128 and ITU-R BS.1770 regulations for loudness | N/A|
| An .mp3 render for your podcast feed |N/A |
| A .wav master file for personal archiving/future work | N/A|
| Full ID3 tagging | N/A |
| Full BWF metadata tagging |N/A  |
| Shownote inclusion | N/A |

That’s a lot of features as standard! But if you fancy a few extra bells and whistles, then by all means I can easily:

|                                      |
| ------------------------------------ |
| Transcribe the Episode               |
| Insert monetized ads in your episode |
| Anything else you fancy              |

For a small service charge (see our [pricing](https://bearsoundstudio.com/#pricing))
