---
title: "Engineers"
weight: 5
---
# Matthew Harazim  

![Matthew Harazim](/img/matt.jpeg) 

Matthew Harazim is the founder, and head technician, of bearSound Studio and has almost a decade's worth of experience in the audio sector. Having studied Music at the University of Aberdeen, and Sound Design at the University of Edinburgh, Matt's experience range from full orchestral composition and recording, to sound synthesis and digital audio implementations. He's an avid podcast fiend, roleplayer and programmer. You'll never find him without a large mug of coffee <i class="fas fa-mug-hot"></i>.  

[<i class="fas fa-globe-europe"></i>](https://www.matthewharazim.co.uk) [<i class="fab fa-instagram"></i>](https://www.instagram.com/matthewharazim) [<i class="fab fa-twitter"></i>](https://twitter.com/bearly_matt) [<i class="fab fa-mastodon"></i>]()
