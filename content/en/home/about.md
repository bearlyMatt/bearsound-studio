---
title: "About"
image: "bearsoundstudio.png"
weight: 1
---

*bearSound Studio is an independent post-production studio for the podcasting and independent radio revolution, operating in Edinburgh, Scotland.*

Established in 2019 by head technician [Matthew Harazim](https://www.bearsoundstudio.com/#engineers), bearSound Studio prides itself on transparent pricing, and offering the *best* quality every time. Unlike many competitors, bearSound studio offers a single tier of post-production service at a base price. That means that *you* get a comprehensive post-production service as **standard**  

The ultimate aim of bearSound Studio is to *proliferate, promote, and produce the best audio-focused content for independent creators*. This means you’ll be in contact with one engineer throughout your entire custom with bearSound Studio, whether that is only the occasional episode, a full season arc, or the whole run, you know how your audio will be produced.
