---
title: "Pricing"
weight: 4
slug: pricing
---
bearSound Studio offers two pricing plans; a **Pay as You Go (PAYG)** service on an episode-by-episode basis, and a **Monthly Subscription**, based on a predetermined number of episodes per month.  

| PAYG | Subscription  
| :--: | :----------: |
|**£1.50** p/m <30mins | **£30** per Episode <30mins | 
|**£2.00** p/m <60mins | **£75** per Episode <60mins |
|**£x.xx** p/m >60mins | **£100** per Episode >60mins |  

---

Further to this, bearSound Studio can offer **Supplementary Extras** on top of the post-production service which include;

| Supplemental Extra | Breakdown | Price |
| :----------------: | :-------: | :---: |
| *Management Service* | Initial set up of RSS feed, deployment to *Spotify*, *Apple Podcasts*, *Pocket Casts* | **£50** One-off payment |
|                        | Episodic deployment fee | **£5 PAYG**/**£2 Subscription** |
|                        | Archival Webstorage | **£15** per anum |
| *Episodic Transcription* | | **£5** per episode <30min |
| *Insertion of Monetized Ads* | | **£1** per episode, per ad |

*As you can see from bearSound Studio's current projects, long-form audio podcasts are a speciality. As such a custom quote will be made upon inquiry and a sample of raw audio is provided to determine the amount of work necessary. As a benchmark **PAYG** customers will have a pricing of roughly **£2+***

*All pricing in £GBP*
