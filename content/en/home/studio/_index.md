---
title: "The Studio"
weight: 6
---

bearSound Studio is a small-scale operation, utilising professional-grade audio equipment to ensure you're sounding the best that you can be, proudly operating out of Edinburgh, Scotland. 

 [Find out what equipment we use on a daily basis.](/equipment)
