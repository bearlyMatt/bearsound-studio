---
title: "Equipment"
weight: 1
---
![DDA Console](/dda.jpg)

We have completely restored a British classic; the DDA Q-Series mixing desk, in a fabulous array of 8 Inputs, 8 Groups, and 6 Outs - meaning all your mixing is done in the analogue domain. Get some classic British sound into your mix.
