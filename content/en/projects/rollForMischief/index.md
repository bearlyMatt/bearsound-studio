---
title: "Roll For Mischief"
weight: 2
resources:
- src: rfm.jpg
  params:
   weight: -100
---
# Roll for Mischief
[Roll For Mischief](https://www.twitch.tv/rollformischief) is a weekly *actual play* Dungeons & Dragons livestream, hosted on Twitch. The catch? Each 'episode' is hosted by a different Dungeon Master, keeping the storytelling fresh, and allowing each member of the party to explore different parts of their personalities. bearSound Studio is currently in the process of prooducing the first season of the podcast, with an early 2021 release.
