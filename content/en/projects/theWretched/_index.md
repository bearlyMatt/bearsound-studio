---
title: The Wretched (Miniseries)
weight: 8
---
# The Wretched
Welcome to the stranded starship *The Wretched*. You've been attacked by an unknown alien entity, the rest of the crew lies dead, the engine has failed, and your distress beacon has no power. 

This is the fate of flight engineer Taneo Wardson. In this four episode miniseries, hosted by *bearly a podcast!*, join Taneo as he struggles against the horror of solitude in space. Each episode follows Taneo's audio-log entries and touches on the destruction wraught by an unknown force; survivor guilt; and naming an alien threat ~~Derek~~. 

You can find *The Wretched* on the *bearly a podcast* RSS feed (and your favourite podcatcher), or on the *bearlyMedia* Bandcamp page, which contains the full <90 minute adventure in audiobook format, and limited edition physical media.

*The Wretched* is an actual-play of the TTRPG of the same name, by developers [Loot the Room](https://loottheroom.itch.io). Please support their fantastic work.
