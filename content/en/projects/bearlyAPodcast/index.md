---
title: "Bearly a Podcast!"
weight: 9
---
## Bearly A Podcast!
Is Matt's homebrew podcast, [Bearly a Podcast](https://www.bearlyapodcast.net) full of crazy conversations with people who know more than him. Starting off as a portfolio piece, Bearly A Podcast! is what happens when you finally say *'I wonder what I could achieve if I actually applied myself'*. ~~SPOILER~~ It's a real mess.
